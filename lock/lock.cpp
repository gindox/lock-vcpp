//
// lock.cpp
//
// Copyright 2008 - 2012 jones@cs.tcd.ie
// Changes made by Grzegorz Kulicki
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software Foundation;
// either version 2 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// 08/11/08 first version (initially as 4BA9 / CS4021 coursework)
// 11/11/12 CriticalSection and testAndSet lock
// 11/11/12 Win32 and x64 versions
// 19/11/12 added testAndTestAndSet lock
// 21/11/12 works with Character Set: "Not Set", "Use Unicode Character Set" or "Use Multi-Byte Character Set"
// 23/11/12 added MCS lock
// 25/11/12 added TestAndSet lock with exponential backoff
//

//
// NB: include "windows.h" in sdtafx.h [quicker since it will be included in the pre-compiled headers]
//

#include "stdafx.h"                             //
#include "time.h"                               // time
#include "conio.h"                              // _getch
#include "intrin.h"                             // intrinsics
#include <iostream>                             // cout
#include <iomanip>                              // setprecision
#using <mscorlib.dll>

using namespace std;                            // cout

#define K                   1024                //
#define GB                  (K*K*K)             //
#define NSECONDS            2                   // run each test for NSECONDS

#define MAXTHREADS                              // max threads
#define MAXKEY              16384               // max key
#define NOPS                1000                // number of operations between tests for exceeding runtime

#define LOCKTYP             7                   // 0:CriticalSection 1:testAndSet 2:testAndTestAndSet 3:testAndSet with exponential backoff 4:ticket 5:MCS 6:myCriticalSection 7:LockFree

//#define SLEEP_OR_PAUSE()  if (nt > ncpus) Sleep(0); else _mm_pause()
#define SLEEP_OR_PAUSE()    _mm_pause()

//
// global variables
//
int ncpus;                                      //
int nt;                                         //
int lineSz;                                     // cache line sz
clock_t tstart;                                 //
int maxkey;                                     //
int maxThread;                                  //
int ri;

HANDLE *threadH;                                // thread handles
long long *cnt;                                 // for computing ops/s
long long *r;                                   // for results

//
// derive from ALIGNEDMA for aligned memory allocation
//
template <class T>
class ALIGNEDMA {
    
public:
    
    void* operator new(size_t);     // override new
    void operator delete(void*);    // override delete
    
};

//
// new
//
template <class T>
void* ALIGNEDMA<T>::operator new(size_t sz)
{
    return _aligned_malloc(sz, lineSz);
}

//
// delete
//
template <class T>
void ALIGNEDMA<T>::operator delete(void *p)
{
    _aligned_free(p);
}

#if LOCKTYP == 0

#define LOCKSTR         "CriticalSection SpinCout=" << SPINCOUNT
#define SPINCOUNT       0
#define DECLARE()       CRITICAL_SECTION cs;
#define INIT()          InitializeCriticalSectionAndSpinCount(&cs, SPINCOUNT);
#define ACQUIRE()       EnterCriticalSection(&cs);
#define RELEASE()       LeaveCriticalSection(&cs);
#define DESTROY()       DeleteCriticalSection(&cs);

#elif LOCKTYP == 1

#define LOCKSTR         "testAndSet lock"
#define DECLARE()       volatile long lock
#define INIT()          lock = 0
#define ACQUIRE()       acquire(&lock)
#define RELEASE()       lock = 0
#define DESTROY()

inline void acquire(volatile long *lock)
{
    while (InterlockedExchange(lock, 1));
}

#elif LOCKTYP == 2

#define LOCKSTR         "testAndTestAndSet lock"
#define DECLARE()       volatile long lock
#define INIT()          lock = 0
#define ACQUIRE()       acquire(&lock)
#define RELEASE()       lock = 0
#define DESTROY()

inline void acquire(volatile long *lock)
{
    while (InterlockedExchange(lock, 1))
        while (*lock == 1)
            _mm_pause();
}

#elif LOCKTYP == 3

#define LOCKSTR         "testAndSet lock with exponential backoff"
#define DECLARE()       volatile long lock
#define INIT()          lock = 0
#define ACQUIRE()       acquire(&lock)
#define RELEASE()       lock = 0
#define DESTROY()

#define DELAY(n)        for (volatile int i = 0; i < n; i++)

inline void acquire(volatile long *lock)
{
    int d = 1;
    while (InterlockedExchange(lock, 1)) {
        DELAY(d);
        d *= 2;
    }
}

#elif LOCKTYP == 4

class TicketLock {
public:
    volatile long ticket;
    volatile long nowServing;
};

#define LOCKSTR         "ticket lock"
#define DECLARE()       TicketLock lock;
#define INIT()          lock.ticket = 0; lock.nowServing = 0
#define ACQUIRE()       acquire(&lock)
#define RELEASE()       lock.nowServing++; Sleep(0);
#define DESTROY()

inline void acquire(TicketLock *lock)
{
    int myTicket = InterlockedExchangeAdd(&lock->ticket, 1);
    while (myTicket != lock->nowServing)
        SLEEP_OR_PAUSE();
}

#elif LOCKTYP == 5

#define LOCKSTR         "MCS lock"
#define DECLARE()       QNode *lock;
#define INIT()          lock = NULL
#define ACQUIRE()       acquire(&lock)
#define RELEASE()       release(&lock)
#define DESTROY()

DWORD tlsIndex;

class QNode : public ALIGNEDMA<QNode> {
public:
    volatile int waiting;
    volatile QNode *next;
};

inline void acquire(QNode **lock)
{
    volatile QNode *i = (QNode*) TlsGetValue(tlsIndex);
    i->next = NULL;
    volatile QNode *pred = (QNode*) InterlockedExchangePointer((PVOID*) lock, (PVOID) i);
    if (pred == NULL)
        return;
    i->waiting = 1;
    pred->next = i;
    while (i->waiting)
        Sleep(0);               // needed if threads > cores
}

inline void release(QNode **lock)
{
    volatile QNode *i = (QNode*) TlsGetValue(tlsIndex);
    volatile QNode *succ;
    if (!(succ = i->next)) {
        if (InterlockedCompareExchangePointer((PVOID*)lock, NULL, (PVOID) i) == i)
            return;
        do {
            succ = i->next;
        } while(!succ);
    }
    succ->waiting = 0;
    Sleep(0);                   // needed if threads > cores
}

#elif LOCKTYP == 6

#define LOCKSTR         "myCriticalSection SpinCout=" << SPINCOUNT
#define SPINCOUNT       0
#define DECLARE()       volatile long lock
#define INIT()          lock = 0
#define ACQUIRE()       acquire(&lock)
#define RELEASE()       lock = 0
#define DESTROY()

inline void acquire(volatile long *lock)
{
    int spin = 0;
    
    while (1) {
        
        if (*lock == 0)
            if (InterlockedExchange(lock, 1) == 0)
                return;
        
        if (spin == SPINCOUNT) {
            Sleep(0);
            spin = 0;
            continue;
        }
        spin++;
    }
}


#elif LOCKTYP == 7

#define LOCKSTR         "lockFreeLinkedList SpinCout=" << SPINCOUNT
#define SPINCOUNT       0
#define DECLARE()
#define INIT()
#define ACQUIRE()
#define RELEASE()
#define DESTROY()
#define CAS(a, e, n)	InterlockedCompareExchangePointer(a, n, e)

#endif


//
// Node
//
class Node : public ALIGNEDMA<Node> {
    
public:
    
    Node(int key, Node *next);          // constructor
    int key;                            // key
    Node *next;                         // next
    
};

//
// List
//
class List : public ALIGNEDMA<Node> {
    
    Node *head;                         // sentinel
	Node *tail;
    DECLARE();                          //
    
public:
    
    List();                             // constructor
    ~List();                            // destructor
    
    int add(int key);                   // add item to list
    int remove(int key);                // remove from ordered list
    
#if LOCKTYP == 7
    
protected:
    
    Node *List::search (int search_key, Node **left_node) {
        Node *left_node_next = NULL, *right_node;
    search_again:
        do {
            
            Node *t = this->head;
            Node *t_next = this->head->next;
            
            do {
                // empty list ?
                if (!is_marked_reference(t_next)) {
                    *left_node = t;
                    left_node_next = t_next;
                }
                t = get_unmarked_reference(t_next);
                
                if (t == this->tail)
                    break;
                t_next = t->next;
            } while (is_marked_reference(t_next) || (t->key < search_key));
            right_node = t;
            
            // lets check adjecent nodes
            if (left_node_next == right_node)
                if ((right_node != this->tail) && is_marked_reference(right_node->next))
                    goto search_again;
                else
                    return right_node;
            
            // remove marked nodes in between, since nodes are not adjacent
            if (CAS((PVOID volatile*) &((*left_node)->next), left_node_next, right_node))
                if ((right_node != this->tail) && is_marked_reference(right_node->next))
                    // start again
                    goto search_again;
                else
                    return right_node;
        } while(true);
    }
    
    bool is_marked_reference(Node* n) {
        unsigned int a = reinterpret_cast<unsigned int>(n);
        return (a%2) == 1;
        /*
         xxxxxxx % 2
         */
    }
    
    Node* get_unmarked_reference(Node*n) {
        unsigned int a = reinterpret_cast<unsigned int>(n);
        return reinterpret_cast<Node*>(a & -2);
        /*
         xxxxxxxx
         11111110
         */
    }
    
    Node* get_marked_reference(Node* n) {
        unsigned int a = reinterpret_cast<unsigned int>(n);
        return reinterpret_cast<Node*>(a | 1);
        /*
         xxxxxxx
         0000001
         */
    }
    
    
#endif
    
};

//
// Node constructor
//
Node::Node(int key, Node *next)
{
    this->key = key;
    this->next = next;
}

//
// List constructor
//
List::List()
{
    head = new Node(0, NULL);
	tail = new Node(0, NULL);
	head->next = tail;
	INIT();
}

//
// List destructor
//
List::~List()
{
    while (head) {
        Node *tmp = head->next;
        delete head;
        head = tmp;
    }
    DESTROY();
}

//
// add to ordered list
//
int List::add(int key)
{
#if LOCKTYP == 7
    
	Node *new_node = new Node(key, NULL);
	Node *right_node, *left_node = NULL;
    
	new_node->key = key;
    
	do {
		right_node = this->search(key, &left_node);
		if ((right_node != tail) && (right_node->key == key))
			return false;
		new_node->next = right_node;
		if (CAS((PVOID volatile*) &left_node->next, right_node, new_node))
			return true;
	} while (true);
    
#else
	Node *p;
    ACQUIRE();
    for (p = head; p->next; p = p->next) {
        if (p->next->key >= key) {
            if (p->next->key == key) {
                RELEASE();
                return 0;
            }
            break;
        }
    }
    p->next = new Node(key, p->next);
    RELEASE();
    return 1;
#endif
}

//
// remove from ordered list
//
int List::remove(int key)
{
#if LOCKTYP == 7
    
	Node *right_node, *right_node_next, *left_node = NULL;
    
	do {
		right_node = search(key, &left_node);
		if ((right_node == tail) || (right_node->key != key))
			return false;
		right_node_next = right_node->next;
		if (!is_marked_reference(right_node_next))
			if (CAS ((PVOID volatile*) &right_node->next, right_node_next, get_marked_reference(right_node_next)))
				//
				break;
	} while (true);
    
	if (!CAS((PVOID volatile*)&(left_node->next), right_node, right_node_next))
		right_node = search (right_node->key, &left_node);
	return true;
    
#else
    ACQUIRE();
    for (Node *p = head; p->next; p = p->next) {
        if (p->next->key >= key) {
            if (p->next->key == key) {
                Node *tmp = p->next;
                p->next = tmp->next;
                RELEASE();
                delete tmp;
                return 1;
            }
            break;
        }
    }
    RELEASE();
    return 0;
#endif
}

List *list;                                     // list

//
// getCPUInfo
//
string getCPUInfo()
{
    string cpuInfo = "unrecognised CPU";
    
    MEMORYSTATUS ms;
    GlobalMemoryStatus(&ms);
    
    //
    // use registry to get processor type
    //
    
    HKEY hKey;
    DWORD len;
    char identifier[256] = "???";
    char processorNameString[256] = "???";
    
    if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, "Hardware\\Description\\System\\CentralProcessor\\0", 0, KEY_READ, &hKey) == ERROR_SUCCESS) {
        
        len = sizeof(identifier);
        RegQueryValueExA(hKey, "Identifier", NULL, NULL, (unsigned char *)&identifier, &len);
        cpuInfo = identifier;
        cpuInfo += "; ";
        len = sizeof(identifier);
        RegQueryValueExA(hKey, "ProcessorNameString", NULL, NULL, (unsigned char *)&identifier, &len);
        cpuInfo += identifier;
        RegCloseKey(hKey);
        
    }
    
    //
    // remove extra space
    //
    size_t pos;
    while ((pos = cpuInfo.find("  ", 0)) != cpuInfo.npos)
        cpuInfo.erase(pos, 1);
    return cpuInfo;
    
}

//
// for CPU cache data returned by cpuid instruction
//
struct CpuIdData {
    int eax;
    int ebx;
    int ecx;
    int edx;
} cd;

//
// look for L1 cache line size (see Intel Application note on CPUID instruction)
//
int lookForL1DataCacheInfo(int v)
{
    if (v & 0x80000000)
        return 0;
    
    int sz = 0;
    
    for (int i = 0; i < 4; i++) {
        switch (v & 0xff) {
            case 0x0a:
            case 0x0c:
            case 0x10:
                return 32;
            case 0x0e:
            case 0x2c:
            case 0x60:
            case 0x66:
            case 0x67:
            case 0x68:
                return 64;
        }
        v >>= 8;
    }
    return 0;
}

//
// getL1DataCacheInfo
//
int getL1DataCacheInfo()
{
    __cpuid((int*) &cd, 2);
    
    if ((cd.eax & 0xff) != 1) {
        cout << "unrecognised cache type: default L= 64" << endl;
        return 64;
    }
    
    int sz;
    
    if (sz = lookForL1DataCacheInfo(cd.eax & ~0xff))
        return sz;
    if (sz = lookForL1DataCacheInfo(cd.ebx))
        return sz;
    if (sz = lookForL1DataCacheInfo(cd.ecx))
        return sz;
    if (sz = lookForL1DataCacheInfo(cd.edx))
        return sz;
    
    cout << "unrecognised cache type: default L= 64" << endl;
    return 64;
}

//
// getDeterministicCacheInfo
//
int getDeterministicCacheInfo()
{
    int type, cores, shared, ways, partitions, lineSz, sets;
    int i = 0;
    while (1) {
        __cpuidex((int*) &cd, 4, i);
        type = cd.eax & 0x1f;
        if (type == 0)
            break;
        cores = ((cd.eax >> 26) & 0x3f) + 1;
        shared = ((cd.eax >> 14) & 0x0fff) + 1;
        //cout << "cores=" << cores << " shared=" << shared << endl;
        cout << "L" << ((cd.eax >> 5) & 0x07);
        cout << ((type == 1) ? " D" : (type == 2) ? " I" : " U");
        ways = ((cd.ebx >> 22) & 0x03ff) + 1;
        partitions = ((cd.ebx) >> 12 & 0x03ff) + 1;
        sets = cd.ecx + 1;
        lineSz = (cd.ebx & 0x0fff) + 1;
        cout << " " << setw(5) << ways*partitions*lineSz*sets/1024 << "K" << " L=" << setw(3) << lineSz << " K=" << setw(3) << ways << " N=" << setw(5) << sets;
        cout << endl;
        i++;
    }
    return lineSz;
}

//
// getCacheLineSz
//
int getCacheLineSz()
{
    __cpuid((int*) &cd, 0);
    if (cd.eax >= 4)
        return getDeterministicCacheInfo();
    return getL1DataCacheInfo();
}

//
// worker thread
//
DWORD WINAPI worker(LPVOID vthread)
{
    int thread = (int) vthread;
    
    srand(thread);      // seed random number generator
    int ops = 0;
    
#if LOCKTYP == 5
    
    QNode *i = new QNode();
    i->waiting = 0;
    i->next = NULL;
    TlsSetValue(tlsIndex, i);
    
#endif
    
    try {
        
        while (1) {
            
            //
            // do some work
            //
            for (int i = 0; i < NOPS; i++) {
                int k = rand() % (maxkey * 2);
                (k & 1) ? list->remove(k >> 1) : list->add(k >> 1);
            }
            ops += NOPS;
            
            //
            // check if runtime exceeded
            //
            if (clock() - tstart > NSECONDS*CLOCKS_PER_SEC)
                break;
            
        }
		System::GC::Collect();
    } catch (std::bad_alloc) {
        
        cout << "Error: run out of memory after " << fixed << setprecision(2) << (double) (clock() - tstart) / 1000.0 << endl;
        
    }
    
    cnt[thread] = ops;
    return 0;
    
}

//
// main
//
int _tmain(int argc, _TCHAR* argv[])
{
    //cout << "sizeof(long) = " << sizeof(long) <<  " sizeof(int) = " << sizeof(int) << endl;
    
    //
    // get computer name
    //
    char name[256];
    DWORD sz = sizeof(name);
    GetComputerNameA(name, &sz);
    
    //
    // get number of CPUs
    //
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    ncpus = si.dwNumberOfProcessors;
    maxThread = 2*ncpus;
    
    //
    // get physical memorysize (RAM)
    //
    ULONGLONG ramSz;                            //
    GetPhysicallyInstalledSystemMemory(&ramSz); // NB: returns KB
    ramSz *= 1024;                              // now bytes
    
    //
    // get date
    //
    time_t t = time(NULL);
    struct tm now;
    char date[256];
    localtime_s(&now, &t);
    strftime(date, sizeof(date), "%d-%b-%Y", &now);
    
    //
    // console output
    //
    cout << name << ((sizeof(size_t) == 8) ? " x64 " : " Win32 ") << LOCKSTR << " NCPUS=" << ncpus << " RAM=" << ramSz / GB << "GB NOPS=" << NOPS << " " << date << endl;
    cout << getCPUInfo().c_str() << endl;
    
    //
    // get cache line size
    //
    lineSz = getCacheLineSz();
    
    threadH = (HANDLE*) _aligned_malloc(maxThread*sizeof(HANDLE), lineSz);      // thread handles
    cnt = (long long*) _aligned_malloc(maxThread*sizeof(long long), lineSz);    // for computing ops/s
    r = (long long*) _aligned_malloc(6*maxThread*sizeof(long long), lineSz);    // for results
    ri = 0;
    
#if LOCKTYP == 5
    
    tlsIndex = TlsAlloc();
    
#endif
    
    for (maxkey = 16; maxkey <= MAXKEY; maxkey *= 4) {
        
        long long opspersec1 = 1;
        
        for (nt = 1; nt <= maxThread; nt *= 2) {
            
            //
            // create an empty list (do this after initialising PT All
            //
            list = new List();
            
            //
            // get start time
            //
            tstart = clock();
            
            //
            // create worker threads
            //
            for (int thread = 0; thread < nt; thread++)
                threadH[thread] = CreateThread(NULL, 0, worker, (LPVOID) thread, 0, NULL);
            
            //
            // wait for ALL worker threads to finish
            //
            WaitForMultipleObjects(nt, threadH, true, INFINITE);
            int runtime = clock() - tstart;
            
            //
            // output results summary on console
            //
            long long total = 0;
            for (int thread = 0; thread < nt; thread++)
                total += cnt[thread];
            long long opspersec = 1000 * total / runtime;
            if (nt == 1)
                opspersec1 = opspersec;
            r[ri++] = opspersec;
            double drt = double(runtime) / 1000.0;
            cout << "key=" << setw(5) << maxkey << " threads=" << setw(2) << nt;
            cout << " rt=" << setw(5) << fixed << setprecision(2) << drt;
            cout << " ops/s=" << setw(8) << opspersec << " relative=" << fixed << setprecision(2) << (double) opspersec / opspersec1 << endl;
            
            //
            // tidy up
            //
            for (int thread = 0; thread < nt; thread++)
                CloseHandle(threadH[thread]);
            delete list;
            
        }
        
    }
    
    cout << "finished" << endl << endl;
    
    //
    // output results so they can easily be pasted into a spredsheet from console window
    //
    for (int i = 0; i < ri; i++)
        cout << r[i] << endl;
    
    //
    // stop DOS window disappearing prematurely
    //
    _getch();
    
    return 0;
    
}

// eof
